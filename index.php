<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/**
 * @author budiamanprasetyo@gmail.com
 * see    : https://<host>/webhook/?site=<name_site>
 * method : POST
 * payload: JSON
 */
class Forward
{
    private $uid            = null;
    private $query_params   = null;   
    private $input          = null;         
    private $body_params    = null;   
    private $headers        = null;        
    private $site           = null;
    private $channel_name   = null;
    private $channel_code   = null;
    private $channel_type   = null;          
    private $config_webhook = array();

    public function __construct()
    {
        $this->uid            = uniqid();
        $this->query_params   = $_GET;
        $this->input          = file_get_contents("php://input"); $this->debug( $this->input, false);
        $this->body_params    = ($this->input == "") ? array() : json_decode($this->input, true);
        $this->headers        = array('Content-Type: application/json');
        $this->site           = ( is_null(@$this->query_params['site']) )? null: strtolower($this->query_params['site']);
        $this->config_webhook = array(
            'test' => array(
                'url'       => 'http://localhost',
                'methode'   => 'POST',
                'auth'      => false,
                'auth_type' => NULL
            )
        );
    }

    /**
     * @method sendForward[
     * Action forward
     * ]
     *
     * @return JSON
     */
    function sendForward()
    {
        $result = array(
            'success' =>  false
        );

        if( isset($this->config_webhook[$this->site]) )
        {
            $url        = @$this->config_webhook[$this->site]['url'];
            $methode    = @$this->config_webhook[$this->site]['methode'];
            $has_auth   = @$this->config_webhook[$this->site]['auth'];
            $type_auth  = @$this->config_webhook[$this->site]['auth_type'];

            $logger = array(
                'uid'     => $this->uid,
                'process' => 'start',
                'url'     => $url,
                'params'  => $this->body_params
            );
            $this->_writePayload( json_encode($logger) );

            $ch  = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->body_params) );
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);

            // set request methode ::
            switch ($methode)
            {
                case 'GET':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methode);
                break;
                case 'POST':
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methode);
                break;
                default:
                    header('Content-Type:application/json');
                    $result['message'] = 'Not found methode!';
                    exit(json_encode( $result ));
                break;
            }

            // Has auth ::
            if( $has_auth === true) {
                switch ( $type_auth )
                {
                    case "Basic":
                        $combine_usr_pswd = $username . ":" . $password;
                        $headers[1]       = array( 'Authorization: Basic ' . base64_encode($combine_usr_pswd) );
                        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                        curl_setopt($ch, CURLOPT_USERPWD, $combine_usr_pswd);
                    break;
                    case "Bearer":
                        $headers[1] = 'Content-Type:application/json';
                    break;
                    default:
                    break;
                }
            }

            // Set Header ::
            curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $output     = curl_exec($ch);
            $info       = curl_getinfo($ch);
            $http_code  = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $error      = curl_error($ch);
            curl_close($ch);

            if( (int)$http_code === 200 || (int)$http_code === 201 )
            {
                $result['success'] = true;
            } else {
                $result['success'] = false;
            }
            $result['message'] = json_decode($output);
        } else {
            $result['message'] = 'Not found module webhook!';
        }

        $logger = array(
            'uid'     => $this->uid,
            'process' => 'end',
            'url'     => @$url,
            'params'  => $this->body_params,
            'result'  => array(
                'info' => @$info,
                'out'  => @$result,
                'code' => @$http_code,
                'error'=> @$error
            )
        );
        $this->_writePayload( json_encode($logger) );

        header('Content-Type: application/json');
        exit( json_encode($result) );
    }

    /**
     * @method _writePayload[
     * Create Log & Write Log
     * ]
     *
     * @param  string $value
     * @return void
     */
    function _writePayload($value='')
    {
        $site = $this->site;
        $channel_name = $this->channel_name;
        $channel_code = $this->channel_code;
        $channel_type = $this->channel_type;
        
        // cek folder log ::
        if( !is_dir(__DIR__."/log/$site" )) {
            @mkdir(__DIR__."/log/$site", 0777, true);
        }
        // cek folder log-sub ::
        if( !is_dir(__DIR__."/log/$site/".date('Ymd') )) {
            @mkdir(__DIR__."/log/$site/".date('Ymd'), 0777, true);
        }

        $path = sprintf("%s/%s_.log",
            __DIR__."/log/$site/".date('Ymd'),
            sprintf("logger_%s_%s_%s", $channel_type, $channel_code, $channel_name)
        );

        $fp     = @fopen($path, 'a');
        if( !$fp ) return false;
        
        fwrite($fp, sprintf("[%s] >> %s\n\n", date('Y-m-d H:i:s'), $value));
        
        if( $fp ) @fclose($fp);

        return true;
    }

    /**
     * @method debug
     * @param  string  $val
     * @param  boolean $mode
     * @return void
     */
    function debug($val="", $mode=true)
    {
        if( $mode === true) die(var_dump( $val ));
    }

    /**
     * @method run[ Run Action ]
     *
     * @return void
     */
    function run()
    {
        $this->sendForward();
    }

/*End-Class ::*/
}

$obj = new Forward;
$obj->run();
